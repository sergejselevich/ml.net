﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Phoneword
{
    public class MainPage : ContentPage
    {
        string translatedNumber;

        Entry phoneNumberText;
        Button translateButton;
        Button callButton;

        public MainPage()
        {
            this.Padding = new Thickness(20, 20, 20, 20);

            phoneNumberText = new Entry
            {
                Text = "1-855-XAMARIN",
            };
            translateButton = new Button
            {
                Text = "Translate"
            };
            callButton = new Button
            {
                Text = "Call",
                IsEnabled = false,
            };

            StackLayout panel = new StackLayout
            {
                Spacing = 15,
                VerticalOptions = LayoutOptions.Center,
                Children =
                {
                    new Label
                    {
                        Text = "Enter a Phoneword:",
                        FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label))
                    },
                    phoneNumberText, translateButton, callButton
                }
            };

            callButton.Clicked += OnCall;
            translateButton.Clicked += OnTranslate;
            this.Content = panel;
        }

        private async void OnCall(object sender, EventArgs e)
        {
            if (await this.DisplayAlert(
                "Dial a Number",
                "Would you like to call " + translatedNumber + "?",
                "Yes",
                "No"))
            {
                // TODO: dial the phone
            }
        }

        private void OnTranslate(object sender, EventArgs e)
        {
            string enteredNumber = phoneNumberText.Text;
            translatedNumber = Core.PhonewordTranslator.ToNumber(enteredNumber);

            if (!string.IsNullOrEmpty(translatedNumber))
            {
                callButton.IsEnabled = true;
                callButton.Text = "Call " + translatedNumber;
            }
            else
            {
                callButton.IsEnabled = false;
                callButton.Text = "Call";
            }
        }
    }
}
