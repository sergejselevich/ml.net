﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using System;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace NetStatusXAM150
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = CrossConnectivity.Current.IsConnected
                ? (Page)new NetworkViewPage()
                : new NoNetworkPage();
        }

        protected override void OnStart()
        {
            base.OnStart();
            CrossConnectivity.Current.ConnectivityChanged += HandleConnectivityChanged;
        }

        void HandleConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            CheckRightPage(e.IsConnected);
            //Type currentPage = this.MainPage.GetType();
            //if (e.IsConnected && currentPage != typeof(NetworkViewPage))
            //    this.MainPage = new NetworkViewPage();
            //else if (!e.IsConnected && currentPage != typeof(NoNetworkPage))
            //    this.MainPage = new NoNetworkPage();
        }

        void CheckRightPage(bool isConnected)
        {
            Type currentPage = this.MainPage.GetType();

            if (isConnected && currentPage != typeof(NetworkViewPage))
                this.MainPage = new NetworkViewPage();
            else if (!isConnected && currentPage != typeof(NoNetworkPage))
                this.MainPage = new NoNetworkPage();
        }


        protected override void OnSleep()
        {
            base.OnSleep();

            if (CrossConnectivity.Current != null)
                CrossConnectivity.Current.ConnectivityChanged -= HandleConnectivityChanged;
        }

        protected override void OnResume()
        {
            base.OnResume();
            
            if (CrossConnectivity.Current != null)
            {
                CheckRightPage(CrossConnectivity.Current.IsConnected);
                CrossConnectivity.Current.ConnectivityChanged += HandleConnectivityChanged;
            }
                
        }
    }
}
