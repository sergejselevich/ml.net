﻿using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using System.Linq;
using Xamarin.Forms;

namespace NetStatusXAM150
{
	public class NetworkViewPage : ContentPage
	{
        public Label ConnectionDetails;

        public NetworkViewPage ()
		{
            ConnectionDetails = new Label
            {
                Text = "Welcome to Xamarin.Forms!",
            };
            Content = ConnectionDetails;
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (CrossConnectivity.Current == null)
                return;

            ConnectionDetails.Text = CrossConnectivity.Current.ConnectionTypes.First().ToString();
            CrossConnectivity.Current.ConnectivityChanged += UpdateNetworkInfo;

        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            if (CrossConnectivity.Current != null)
                CrossConnectivity.Current.ConnectivityChanged -= UpdateNetworkInfo;
        }

        private void UpdateNetworkInfo(object sender, ConnectivityChangedEventArgs e)
        {
            if (CrossConnectivity.Current != null && CrossConnectivity.Current.ConnectionTypes != null)
            {
                var connectionType = CrossConnectivity.Current.ConnectionTypes.FirstOrDefault();
                ConnectionDetails.Text = connectionType.ToString();
            }
        }
    }
    
}